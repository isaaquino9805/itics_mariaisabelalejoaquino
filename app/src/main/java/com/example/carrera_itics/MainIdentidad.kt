package com.example.carrera_itics

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class MainIdentidad : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        title = "Identidad"
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main_identidad)

        val boton1 = findViewById<Button> (R.id.btnIrMision)
        boton1.setOnClickListener { val intent = Intent (this, Mision ::class.java)
            startActivity(intent)
        }
        val boton2 = findViewById<Button> (R.id.btnIrVision)
        boton2.setOnClickListener { val intent = Intent (this, Vision ::class.java)
            startActivity(intent)
        }
        val boton3 = findViewById<Button> (R.id.btnIrObjetivo)
        boton3.setOnClickListener { val intent = Intent (this, Objetivodelprograma ::class.java)
            startActivity(intent)
        }
        val boton4 = findViewById<Button> (R.id.btnIrCampo)
        boton4.setOnClickListener { val intent = Intent (this, Campodeaccion ::class.java)
            startActivity(intent)
        }
        val boton5 = findViewById<Button> (R.id.btnIrModulo)
        boton5.setOnClickListener { val intent = Intent (this, Modulodeespecialidad ::class.java)
            startActivity(intent)
        }
    }
}