package com.example.carrera_itics;

import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.carrera_itics.adapter.RecyclerAdapter;
import com.example.carrera_itics.model.ItemList;

import java.util.ArrayList;
import java.util.List;

public class Reticula extends AppCompatActivity {

    private RecyclerView rvLista;
    private RecyclerAdapter adapter;
    private List<ItemList> items;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reticula);
        initViews();
        initValues();

    }

    private void initViews() {
        rvLista = findViewById(R.id.rvLista);
    }

    private void initValues() {
        LinearLayoutManager manager = new LinearLayoutManager(this);
        rvLista.setLayoutManager(manager);
        items = getItems();
        adapter = new RecyclerAdapter(items);
        rvLista.setAdapter(adapter);

    }

    private List<ItemList> getItems() {

        List<ItemList> itemLists = new ArrayList<>();
        itemLists.add(new ItemList("PRIMER SEMESTRE", "CALCULO DIFERENCIAL.", R.drawable.calculodiferencial));
        itemLists.add(new ItemList("PRIMER SEMESTRE", "FUNDAMENTOS DE PROGRAMACION.", R.drawable.fundamentosprogramacion));
        itemLists.add(new ItemList("PRIMER SEMESTRE", "MATEMATICAS DISCRETAS.", R.drawable.matematicasdiscretas));
        itemLists.add(new ItemList("PRIMER SEMESTRE", "INTRODUCCION A LAS TICS.", R.drawable.introduccionalastics));
        itemLists.add(new ItemList("PRIMER SEMESTRE", "TALLER DE ETICA.", R.drawable.tallerdeetica));
        itemLists.add(new ItemList("PRIMER SEMESTRE", "FUNDAMENTOS DE INVESTIGACION.", R.drawable.fundamentosdeinvestigacion));
        itemLists.add(new ItemList("SEGUNDO SEMESTRE", "CALCULO INTEGRAL.", R.drawable.calculointegral));
        itemLists.add(new ItemList("SEGUNDO SEMESTRE", "PROGRAMACION ORIENTADA A OBJETOS.", R.drawable.poo));
        itemLists.add(new ItemList("SEGUNDO SEMESTRE", "MATEMATICAS DISCRETAS II.", R.drawable.matematicasdiscretas2));
        itemLists.add(new ItemList("SEGUNDO SEMESTRE", "PROBABILIDAD Y ESTADISTICA.", R.drawable.probabilidadyestadistica));
        itemLists.add(new ItemList("SEGUNDO SEMESTRE", "CONTABILIDAD Y COSTOS.", R.drawable.contabilidadycostos));
        itemLists.add(new ItemList("SEGUNDO SEMESTRE", "ADMINISTRACION GERENCIAL.", R.drawable.administraciongerencial));
        itemLists.add(new ItemList("TERCER SEMESTRE", "MATEMATICAS APLICADAS A COMPUTACION.", R.drawable.matematicasaplicadasalacomputacion));
        itemLists.add(new ItemList("TERCER SEMESTRE", "ESTRUCTURA Y ORGANIZACION DE DATOS.", R.drawable.estrcucturaorganizaciondedatos));
        itemLists.add(new ItemList("TERCER SEMESTRE", "DESARROLLO SUSTENTABLE.", R.drawable.desarrollosustentable));
        itemLists.add(new ItemList("TERCER SEMESTRE", "FUNDAMENTOS DE BASE DE DATOS.", R.drawable.fundamentosdebasededatos));
        itemLists.add(new ItemList("TERCER SEMESTRE", "ELECTRICIDAD Y MAGNETISMO.", R.drawable.electricidadymagnetismo));
        itemLists.add(new ItemList("TERCER SEMESTRE", "ALGEBRA LINEAL.", R.drawable.algebralineal));
        itemLists.add(new ItemList("CUARTO SEMESTRE", "ANALISIS DE SEÑALES Y SISTEMAS DE COMUNICACION.", R.drawable.analisisdese));
        itemLists.add(new ItemList("CUARTO SEMESTRE", "PROGRAMACION II.", R.drawable.programacion2));
        itemLists.add(new ItemList("CUARTO SEMESTRE", "MATEMATICAS PARA DECISIONES.", R.drawable.matematicasparadecisiones));
        itemLists.add(new ItemList("CUARTO SEMESTRE", "TALLER DE BASE DE DATOS.", R.drawable.tallerdebasededatos));
        itemLists.add(new ItemList("CUARTO SEMESTRE", "CIRTCUITOS ELECTRICOS Y ELECTRONICOS.", R.drawable.circuitoselectricosyelectronicos));
        itemLists.add(new ItemList("CUARTO SEMESTRE", "INGENIERIA DEL SOFTWARE.", R.drawable.ingenieriadelsoftware));
        itemLists.add(new ItemList("QUINTO SEMESTRE", "FUNDAMENTOS DE REDES.", R.drawable.fundamentosderedes));
        itemLists.add(new ItemList("QUINTO SEMESTRE", "TELECOMUNICACIONES.", R.drawable.telecomunicaciones));
        itemLists.add(new ItemList("QUINTO SEMESTRE", "ADMINISTRACION DE PROYECTOS.", R.drawable.admonproyectos));
        itemLists.add(new ItemList("QUINTO SEMESTRE", "BASES DE DATOS DISTRIBUIDAS.", R.drawable.bddistribuidas));
        itemLists.add(new ItemList("QUINTO SEMESTRE", "ARQUITECTURA DE COMPUTADORAS.", R.drawable.arquitecturadecomputadoras));
        itemLists.add(new ItemList("QUINTO SEMESTRE", "TALLER DE INGENIERIA DEL SOFTWARE.", R.drawable.tallerdeingdelsoftware));
        itemLists.add(new ItemList("QUINTO SEMESTRE", "SISTEMAS OPERATIVOS.", R.drawable.so));
        itemLists.add(new ItemList("SEXTO SEMESTRE", "REDES DE COMPUTADORAS.", R.drawable.redesdecomputadoras));
        itemLists.add(new ItemList("SEXTO SEMESTRE", "AUDITORIA EN LAS TICS.", R.drawable.auditoriaenlastics));
        itemLists.add(new ItemList("SEXTO SEMESTRE", "DESARROLLO DE EMPRENDEDORES.", R.drawable.desarrollodeemprendedores));
        itemLists.add(new ItemList("SEXTO SEMESTRE", "NEGOCIOS ELECTRONICOS.", R.drawable.negocioselectronicos));
        itemLists.add(new ItemList("SEXTO SEMESTRE", "PROGRAMACION WEB.", R.drawable.programacionweb));
        itemLists.add(new ItemList("SEXTO SEMESTRE", "TECNOLOGIAS INALAMBRICAS.", R.drawable.tecnologiasinalambricas));
        itemLists.add(new ItemList("SEXTO SEMESTRE", "SISTEMAS OPERATIVOS II.", R.drawable.so2));
        itemLists.add(new ItemList("SEXTO SEMESTRE", "REDES EMERGENTES.", R.drawable.redesemergentes));
        itemLists.add(new ItemList("SEXTO SEMESTRE", "TELECOMUNICACIONES AVANZADAS.", R.drawable.telecomunicacionesavanzadas));
        itemLists.add(new ItemList("SEXTO SEMESTRE", "HARDWARE RECONFIGURABLE.", R.drawable.hardwarereconfigurable));
        itemLists.add(new ItemList("SEXTO SEMESTRE", "NEGOCIOS ELECTRONICOS II.", R.drawable.negocioselectronicos2));
        itemLists.add(new ItemList("SEXTO SEMESTRE", "DESARROLLO DE APP MOVILES.", R.drawable.desarrollodeappmovil));
        itemLists.add(new ItemList("SEXTO SEMESTRE", "TALLER DE INVESTIGACION I.", R.drawable.tallerdeinv1));
        itemLists.add(new ItemList("SEXTO SEMESTRE", "COMPUTO SUAVE.", R.drawable.computosuave));
        itemLists.add(new ItemList("OCTAVO SEMESTRE", "ADMINISTRACION Y SEGURIDAD DE REDES.", R.drawable.admonyseguridadderedes));
        itemLists.add(new ItemList("OCTAVO SEMESTRE", "INTERACCION HUM-COMP.", R.drawable.interaccionhumanacomp));
        itemLists.add(new ItemList("OCTAVO SEMESTRE", "SISTEMAS EMBEBIDOS.", R.drawable.sistemasembebidos));
        itemLists.add(new ItemList("OCTAVO SEMESTRE", "INGENIERIA DEL CONOCIMIENTO.", R.drawable.ingenieriadeconocimiento));
        itemLists.add(new ItemList("OCTAVO SEMESTRE", "SISTEMAS DISTRIBUIDOS MOVILES.", R.drawable.sistemasdistribuidosmoviles));
        itemLists.add(new ItemList("OCTAVO SEMESTRE", "TALLER DE INVESTIGACION II.", R.drawable.tallerdeinv2));
        itemLists.add(new ItemList("NOVENO SEMESTRE", "RESIDENCIA PROFESIONAL.", R.drawable.residencias));
        return itemLists;
    }
}





















