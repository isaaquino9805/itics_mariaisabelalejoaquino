package com.example.carrera_itics

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button

class Menu : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        title = "Menú"
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_menu)

        val boton1 = findViewById<Button> (R.id.btnIrReticula)
        boton1.setOnClickListener { val intent = Intent (this, Reticula::class.java)
            startActivity(intent)
        }
        val boton2 = findViewById<Button> (R.id.btnIrIdentidad)
        boton2.setOnClickListener { val intent = Intent (this, MainIdentidad::class.java)
            startActivity(intent)
        }
        val boton3 = findViewById<Button> (R.id.btnIrPerfiles)
        boton3.setOnClickListener { val intent = Intent (this, Perfiles::class.java)
            startActivity(intent)
        }


    }
}